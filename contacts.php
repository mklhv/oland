<?php
define('myeshop', true);
include("include/db_connect.php");
include("functions/functions.php");
session_start();
include("include/auth_cookie.php");

if ($_POST["send_message"])
{
    $error = array();

    if (!$_POST["feed_name"]) $error[] = "Укажите своё имя";

    if(!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i",trim($_POST["feed_email"])))
    {
        $error[] = "Укажите корректный E-mail";
    }

    if (!$_POST["feed_subject"]) $error[] = "Укажите тему письма!";
    if (!$_POST["feed_text"]) $error[] = "Укажите текст сообщения!";

    if (strtolower($_POST["reg_captcha"]) != $_SESSION['img_captcha'])
    {
        $error[] = "Неверный код с картинки!";
    }


    if (count($error))
    {
        $_SESSION['message'] = "<p id='form-error'>".implode('<br />',$error)."</p>";

    }else
    {
        send_mail($_POST["feed_email"],
            'oland@oland.in.ua',
            $_POST["feed_subject"],
            'От: '.$_POST["feed_name"].'<br/>'.$_POST["feed_text"]);

        $_SESSION['message'] = "<p id='form-success'>Ваше сообщение успешно отправлено!</p>";

    }

}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>ТОВ НВЦ "ОЛАНД" - Контакты</title>
    <link rel="shortcut icon" href="images/icon.ico" type="image/x-icon">
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
    <link href="trackbar/trackbar.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lobster&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="/js/jquery-1.8.2.min.js"></script>
    <!--<script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-1.9.1.js"></script> -->
    <script type="text/javascript" src="/js/jcarousellite_1.0.1.js"></script>
    <script type="text/javascript" src="/js/shop-script.js"></script>
    <script type="text/javascript" src="/js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="/trackbar/jquery.trackbar.js"></script>
    <script type="text/javascript" src="/js/TextChange.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>

    <style>
        #block-content > p {
            text-indent: 20px; /* Отступ первой строки в пикселах */
        }
        html{
            overflow: auto;
        }

        /* СТОРІНКА "КОНТАКТЫ" (ТАБЛИЦА) */

        table {
            background: #fafafa; /* фон таблицы */
            background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
            background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
            font-family: Arial, "Helvetica CY", "Nimbus Sans L", sans-serif;    /* шрифт */
            color: #666;    /* цвет текста */
            text-shadow: 1px 1px 0px #fff;  /* тень текста */
            border:#ccc 1px solid;    /* цвет бордюра */
            border-collapse: collapse;    /* игнор sellspasing, бордюр в одну линию */

            /* == кроссбраузерная тень таблицы ==*/
            -moz-box-shadow: 0 1px 2px #d1d1d1;
            -webkit-box-shadow: 0 1px 2px #d1d1d1;
            box-shadow: 0 1px 2px #d1d1d1;
            width: 470px;   /* добавляем фиксированную ширину */
        }

        .scroller {
            width: 472px;
            height: auto;
            /* height: 220px; */
            margin: 0 auto;
            padding-bottom: 20px;
            /* overflow-y: scroll;   /* прокрутка по оси y */
        }

        thead {
            font-weight: bold;    /* жирный шрифт */
            padding:21px 25px 22px 25px;
            border-top:1px solid #fafafa;
            border-bottom:1px solid #e0e0e0;
            text-align:center;
        }
        /* фон заголовка */
        thead td{
            border: none;
            background: rgb(252,255,244); /* старые браузеры */
            background: -moz-linear-gradient(top,  rgba(252,255,244,1) 0%, rgba(223,229,215,1) 40%, rgba(179,190,173,1) 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top,  rgba(252,255,244,1) 0%,rgba(223,229,215,1) 40%,rgba(179,190,173,1) 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom,  rgba(252,255,244,1) 0%,rgba(223,229,215,1) 40%,rgba(179,190,173,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfff4', endColorstr='#b3bead',GradientType=0 ); /* IE6-9 */
        }

        td {
            padding:12px; /* отступы ячеек */
            border-top: 1px solid #ffffff;
            border-bottom:1px solid #e0e0e0;
            border-left: 1px solid #e0e0e0;
        }

        tr:hover {
            background: #f2f2f2;
        }/*после знака решетки задайте свой цвет*/

        .odd {
            background: #f6f6f6; /* горизонтальная зебра */
            background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
            background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
        }
    </style>
</head>
<body style="background-color: #f1f1f1;">
<?php
defined('myeshop') or die('Доступ запрещён!');
?>
<!-- Основной верхний блок.  -->
<div id="block-header" style="height: 60px;">
    <!-- Верхний блок с навигацией -->
    <div id="header-top-block">
        <!-- Список с навигацией -->
        <ul id="header-top-menu">
            <li class="no-hover-logo" style="flex: 1.5 0 auto; min-width: 249px; max-width: 280px;"><a href="index.php" class="img-responsive" id="img-logo" alt="Главная" title="Главная" style="width: 200px; height: 60px; align-self: auto; margin-left: 20px;" onMouseOver="this.style.textDecoration='none';"onmouseout="this.style.textDecoration='none';"></a></li>
            <li class="ico-home"><a href="main.php"></a></li>
            <li><a href="index.php">Товары</a></li>
            <li><a href="contacts.php">Контакты</a></li>
            <li><a href="services.php">Услуги</a></li>
        </ul>
    </div>
</div>

<div id="flex-container" style="background-color: #f1f1f1;">
    <iframe src="https://www.google.com/maps/d/embed?mid=1HkRkq63Od2f7_eyyxeA1CZTqi7U" width="100%" height="480"></iframe>
    <div id="flex-container-tovar" style="width: 920px; /*background-color: #4d6a79;*/ color: #f8f8f8;">
        <div id="block-content" style="margin: 10px; text-align: justify; width:100%;     box-shadow: 0 1px 4px rgba(0, 0, 0, .3), -23px 0 20px -23px rgba(0, 0, 0, .8), 23px 0 20px -23px rgba(0, 0, 0, .8), 0 0 40px rgba(0, 0, 0, .1) inset;
    background-color: #4d6a79;">
            <div class="container-cont">
                <h1 align="center" class="about-header">Режим работы</h1>
                <div class="scroller">
                    <table border="1" cellpadding="8" cellspacing="3">
                        <thead style="font-size: 18px;">
                        <td>Дни недели</td>
                        <td>Время работы</td>
                        </thead>
                        <tr>
                            <td style="font-size: 16px;">Понедельник</td>
                            <td align="center" valign="middle" rowspan="5" style="color: #4CAF50; font-size: 32px; font-weight: bold;">8:00 - 17:00</td>
                        </tr>
                        <tr class="odd">
                            <td style="font-size: 16px;">Вторник</td>
                        </tr>
                        <tr>
                            <td style="font-size: 16px;">Среда</td>
                        </tr>
                        <tr class="odd">
                            <td style="font-size: 16px;">Четверг</td>
                        </tr>
                        <tr>
                            <td style="font-size: 16px;">Пятница</td>
                        </tr>
                        <tr class="odd">
                            <td style="font-size: 16px;">Суббота</td>
                            <td align="center" valign="middle" rowspan="2" style="font-size: 32px; font-weight: bold; color: #F44336;">Выходной</td>
                        </tr>
                        <tr>
                            <td style="font-size: 16px;">Воскресение</td>
                        </tr>
                    </table>
                </div>


                <h1 align="center" class="about-header">Контакты</h1>
                <div class="scroller">
                    <table border="1" cellpadding="8" cellspacing="3">
                        <thead style="font-size: 18px;">
                        <td>Номер телефона</td>
                        <td>Контактные лица</td>
                        </thead>
                        <tr>
                            <td style="font-size: 16px;">(0-41-43)14-15-17</td>
                            <td align="center" valign="middle" rowspan="4" style="color: #4CAF50; font-size: 32px; font-weight: bold;">Андрей, Олег</td>
                        </tr>
                        <tr class="odd">
                            <td style="font-size: 16px;">+380679099687</td>
                        </tr>
                        <tr>
                            <td style="font-size: 16px;">+380936634791</td>
                        </tr>
                    </table>
                </div>

                <p style="margin: 10px; text-align: center; font:20px sans-serif;">
                    Для того чтобы <span style="color: cornsilk; font-weight: bold;">СВЯЗАТЬСЯ С НАМИ</span> вы вожете заполнить форму обратной связи которую вы видите ниже.
                </p>

                <?php
                if(isset($_SESSION['message']))
                {
                    echo $_SESSION['message'];
                    unset($_SESSION['message']);
                }
                ?>
                <form method="post">
                    <div id="block-feedback">
                        <ul id="feedback">

                            <li><label>Ваше Имя</label><input type="text" name="feed_name" class="form-control" /></li>
                            <li><label>Ваш E-mail</label><input type="text" name="feed_email" class="form-control" /></li>
                            <li><label>Тема</label><input type="text" name="feed_subject" class="form-control" /></li>
                            <li><label>Текст сообщения</label><textarea name="feed_text" class="form-control"></textarea></li>

                            <li>
                                <label for="reg_captcha">Защитный код</label>
                                <div id="block-captcha">
                                    <img src="/reg/reg_captcha.php" />
                                    <input type="text" class="form-control" name="reg_captcha" id="reg_captcha" style="margin-left:10px;"/>
                                    <p id="reloadcaptcha">Обновить</p>
                                </div>
                            </li>
                            <p align="right"><input type="submit" name="send_message" class="btn btn-success" id="form_submit" style="height: 30px;
                             width: 100px;
                             font: 17px sans-serif;
                             margin-left: 0;
                             margin-top:10px;
                             border-left: 1px solid #4cae4c;
                             border-top: 1px solid #4cae4c;
                             border-right: 1px solid #4cae4c;
                             border-bottom: 1px solid #4cae4c;
                             padding: 6px;2 " />
                            </p>

                        </ul>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<?php
include("include/block-footer-client.php");
?>
</body>
</html>