<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="../images/icon.ico" type="image/x-icon">
    <link href="../css/reset.css" rel="stylesheet" type="text/css" />
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" />
    <title>Оплата прошла успешно!</title>
    <style>
        div > button > a{
            color: #ffffff;
            text-decoration: none;
            font-size: 20px;
            width: 100%;
        }
        div > button > a:hover,a:focus{
            color: #000000;
            text-decoration: none;
        }
        div > hr{
            color:lightgrey;
            width: 75%;
            border-top: 2px solid #eee;
        }
        div > img{
            width: 500px;
            height: 500px;
            align-self: center;
        }
        div > button {
            width: 35%; height: 35px; align-self: center;
        }
    </style>
</head>
<body>
<div class="container" style="display: flex; flex-direction: column; justify-content: center;">
    <img src="../images/pay_ok.jpg" alt="Платеж успешно обработан!!" style="">
    <h1 align="center">Ваш платеж успешно обработан!!</h1>
    <hr style="">
    <h3 align="center">Нажмите на кнопку чтобы перейти на сайт</h3>
    <button class="btn-success" style=""><a href="../index.php" style="">Перейти на сайт</a></button>
</div>
</body>
</html>