<?php
   define('myeshop', true);
   include("include/db_connect.php");
   include("functions/functions.php");
   session_start();
   include("include/auth_cookie.php");

$sorting = $_GET["sort"];

switch ($sorting)
{
    case 'price-asc';
    $sorting = 'price ASC';
    $sort_name = 'От дешевых к дорогим';
    break;

    case 'price-desc';
    $sorting = 'price DESC';
    $sort_name = 'От дорогих к дешевым';
    break;

    case 'popular';
    $sorting = 'count DESC';
    $sort_name = 'Популярное';
    break;

    case 'news';
    $sorting = 'datetime DESC';
    $sort_name = 'Новинки';
    break;

    case 'brand';
    $sorting = 'brand';
    $sort_name = 'Новинки';
    break;

    default:
    $sorting = 'products_id DESC';
    $sort_name = 'Нет сортировки';
    break;
}


?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>ТОВ НВЦ "ОЛАНД" - О нас</title>
    <link rel="shortcut icon" href="images/icon.ico" type="image/x-icon">
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
    <link href="trackbar/trackbar.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lobster&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="/js/jquery-1.8.2.min.js"></script>
    <!--<script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-1.9.1.js"></script> -->
    <script type="text/javascript" src="/js/jcarousellite_1.0.1.js"></script>
    <script type="text/javascript" src="/js/shop-script.js"></script>
    <script type="text/javascript" src="/js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="/trackbar/jquery.trackbar.js"></script>
    <script type="text/javascript" src="/js/TextChange.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
    <style>
        #block-content > p {
            text-indent: 20px; /* Отступ первой строки в пикселах */
            }
        #block-content > #img-box {
            text-indent: 20px; /* Отступ первой строки в пикселах */
            margin: 0 auto;
            display: flex;
            justify-content: center;
        }
        #block-content > #img-box > img{
            width: 45%;
            height: 350px;
            display: inline-flex;
            border: 4px solid #2196F3;
        }
        #block-content > #img-box > img:last-child{
            margin-left: 10px;

        }
    </style>
</head>
<body style="background-color: #f1f1f1;">
<?php
    defined('myeshop') or die('Доступ запрещён!');
?>
<!-- Основной верхний блок.  -->
<div id="block-header" style="height: 60px;">
<!-- Верхний блок с навигацией -->
<div id="header-top-block">
<!-- Список с навигацией -->
<ul id="header-top-menu">
<li class="no-hover-logo" style="flex: 1.5 0 auto; min-width: 249px; max-width: 280px;"><a href="index.php" class="img-responsive" id="img-logo" alt="Главная" title="Главная" style="width: 200px; height: 60px; align-self: auto; margin-left: 20px;" onMouseOver="this.style.textDecoration='none';"onmouseout="this.style.textDecoration='none';"></a></li>
<li class="ico-home"><a href="main.php"></a></li>
<li><a href="index.php">Товары</a></li>
<li><a href="contacts.php">Контакты</a></li>
<li><a href="services.php">Услуги</a></li>
</ul>
</div>
</div>

<div id="flex-container" style="background-color: #f1f1f1;">
<iframe src="https://www.google.com/maps/d/embed?mid=1HkRkq63Od2f7_eyyxeA1CZTqi7U" width="100%" height="480"></iframe>
<div id="flex-container-tovar" style="width: 920px; /*background-color: #4d6a79;*/ color: #f8f8f8;">
<div id="block-content" style="margin: 10px; text-align: justify; width:100%;     box-shadow: 0 1px 4px rgba(0, 0, 0, .3), -23px 0 20px -23px rgba(0, 0, 0, .8), 23px 0 20px -23px rgba(0, 0, 0, .8), 0 0 40px rgba(0, 0, 0, .1) inset;
    background-color: #4d6a79;">
    <h1 class="about-header">О нас</h1>
    <p style="margin: 10px; text-align: justify; font:20px sans-serif;">
        <span style="color: cornsilk; font-weight: bold;">ТОВ НВЦ «ОЛАНД»</span> занимается поставками сварочных электродов уже на протяжении 10 лет. За это время многие предприятия, с которыми мы сотрудничаем, убедились в том, что мы являемся надежными партнерами. На протяжении долгих лет, мы поставляем продукцию, только самого высокого качества, которая соответствует жестким требованиям ГОСТ и техническим условиям, и используются для проведения ответственных сварочных работ.
    </p>
    <p style="margin: 10px; text-align: justify; font:20px sans-serif;">
       Среди сварочных электродов, которые мы предлагаем: электроды для сварки высоколегированных сталей и сплавов, электроды для сварки низколегированных и теплоустойчивых сталей, электроды для сварки жаростойких и аустенитных сталей, электроды для сварки цветных металлов и чугуна, а также другие виды сварочных электродов и проволоки.
    </p>
    <p id="img-box"><img src="images/elektrod.jpg" alt=""><img src="images/elektrod2.jpg" alt=""></p>

    <p style="margin: 10px; text-align: justify; font:20px sans-serif;">
       К положительным моментам сотрудничества с нашим предприятием хотелось бы отнести следующие: конкурентоспособная цена при высоком качестве поставляемой продукции;
    </p>
    <ul style="list-style-type: circle; padding-left: 55px; font: 20px sans-serif; color: cornsilk;">
        <li>отгрузка продукции может быть осуществлена день в день, при условии наличия товара на складе;</li>
        <li>сроки оплаты за отгруженную продукцию будут уточняться при заключении договора;</li>
        <li>большой ассортимент продукции на складе.</li>
    </ul>
    <p style="margin: 10px; text-align: justify; font:20px sans-serif;">
        Мы будем искренне рады, увидеть Ваше предприятие среди своих партнеров. Надеемся на долгосрочное и взаимовыгодное сотрудничество. Для изучения цен и ассортимента нашей продукции, предлагаем Вам ознакомиться с нашим ассортиментом в разделе товары. В случае производственной необходимости, вы можете получить образцы сварочных электродов для проверки их в лабораторных условиях.<br>
        <span style="color: cornsilk; font-weight: bold">Так же наше предприятие предоставляет услуги по сварочным работам.</span>
    </p>
</div>
</div>
</div>
<?php
include("include/block-footer-client.php");
?>
</body>
</html>