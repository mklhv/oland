<?php
defined('myeshop') or die('Доступ запрещён!');
?>
<div id="flex-footer-content" style="background: linear-gradient(#f5f5f5, rgb(77, 106, 121));">
    <div id="flex-footer-content-container">
        <div id="block-footer" style="width: 920px;">
            <!-- <div id="bottom-line"></div> -->
            <div id="footer-phone">

                <h4>Служба поддержки</h4>
                <h3> (+380)679-099-687</h3>

                <p>
                    Режим работы:<br />
                    Будние дни: с 8:00 до 17:00<br />
                    Суббота, Воскресенье - выходные
                </p>
            </div>

            <div class="footer-list">
                <p>Сервис и Помощь</p>
                <ul>
                    <li><a href="help_buy.pdf" target="_blank" >Как сделать заказ</a></li>
                    <li><a href="help_payment.php" target="_blank" >Способы оплаты</a></li>
                    <li><a href="help_public.pdf" target="_blank" >Публичная оферта</a></li>
                </ul>

            </div>

            <div class="footer-list">
                <p>О Компании:</p>
                <ul>
                    <li><a href="main.php" target="_blank">О нас</a></li>
                    <li><a href="contacts.php" target="_blank">Контакты</a></li>
                </ul>

            </div>

            <div class="footer-list">
                <p>Навигация</p>
                <ul>
                    <li><a href="index.php" target="_blank">Главная страница</a></li>
                    <li><a href="contacts.php" target="_blank">Обратная связь</a></li>
                </ul>
                <p>Рассказать о сайте</p>

                <script type="text/javascript">(function() {
                        if (window.pluso)if (typeof window.pluso.start == "function") return;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                        var h=d[g]('head')[0] || d[g]('body')[0];
                        h.appendChild(s);
                    })();</script>
                <div class="pluso" data-options="small,square,line,horizontal,nocounter,theme=08" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print" data-background="#ebebeb"></div>

            </div>

        </div>
    </div>
</div>