-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 25 2017 г., 23:41
-- Версия сервера: 5.5.53
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `db_oland`
--

-- --------------------------------------------------------

--
-- Структура таблицы `buy_products`
--

CREATE TABLE `buy_products` (
  `buy_id` int(11) NOT NULL,
  `buy_id_order` int(11) NOT NULL,
  `buy_id_product` int(11) NOT NULL,
  `buy_count_product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `buy_products`
--

INSERT INTO `buy_products` (`buy_id`, `buy_id_order`, `buy_id_product`, `buy_count_product`) VALUES
(1, 1, 17, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `cart_id_product` int(11) NOT NULL,
  `cart_price` int(11) NOT NULL,
  `cart_count` int(11) NOT NULL DEFAULT '1',
  `cart_datetime` datetime NOT NULL,
  `cart_ip` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cart`
--

INSERT INTO `cart` (`cart_id`, `cart_id_product`, `cart_price`, `cart_count`, `cart_datetime`, `cart_ip`) VALUES
(1, 0, 0, 1, '2017-05-25 16:04:54', '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `brand` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `type`, `brand`) VALUES
(1, 'mobile', 'Для сварки нержавеющих сталей'),
(2, 'mobile', 'Вольфрамовые'),
(3, 'mobile', 'Для наплавки'),
(4, 'mobile', 'Для чугуна'),
(5, 'mobile', 'Для сварки меди и сплавов'),
(6, 'mobile', 'Для сварки углеродистых и низколегированных конструкционных сталей'),
(7, 'mobile', 'Для сварки жаростойких и жаропрочных сталей и сплавов'),
(8, 'mobile', 'Для сварки теплоустойчивых сталей'),
(9, 'notebook', 'Для сварки углеродистых и низкоуглеродистых сплавов'),
(10, 'notebook', 'Для сварки нержавеющих сплавов');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `text`, `date`) VALUES
(1, 'Бонус на счет при покупке смартфонов Nokia', 'Купите акционный смартфон Nokia и получите на свой счет до 1000 рублей!', '2013-08-23'),
(2, 'Бонус на счет при покупке планшета с 3G-модулем', 'Получите до 1000 бонусных рублей на счет при покупке планшета в МТС!', '2013-08-23');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `order_datetime` datetime NOT NULL,
  `order_confirmed` varchar(10) NOT NULL,
  `order_dostavka` varchar(255) NOT NULL,
  `order_pay` varchar(50) NOT NULL,
  `order_type_pay` varchar(100) NOT NULL,
  `order_fio` text NOT NULL,
  `order_address` text NOT NULL,
  `order_phone` varchar(50) NOT NULL,
  `order_note` text NOT NULL,
  `order_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`order_id`, `order_datetime`, `order_confirmed`, `order_dostavka`, `order_pay`, `order_type_pay`, `order_fio`, `order_address`, `order_phone`, `order_note`, `order_email`) VALUES
(1, '2017-05-25 12:23:46', '', 'По почте', '', '', 'ввввв', 'г.ПАВІП', '2423324124', 'уцацуа', 'suchka@mail.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `reg_admin`
--

CREATE TABLE `reg_admin` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `fio` text NOT NULL,
  `role` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `view_orders` int(11) NOT NULL DEFAULT '0',
  `accept_orders` int(11) NOT NULL DEFAULT '0',
  `delete_orders` int(11) NOT NULL DEFAULT '0',
  `add_tovar` int(11) NOT NULL DEFAULT '0',
  `edit_tovar` int(11) NOT NULL DEFAULT '0',
  `delete_tovar` int(11) NOT NULL DEFAULT '0',
  `accept_reviews` int(11) NOT NULL DEFAULT '0',
  `delete_reviews` int(11) NOT NULL DEFAULT '0',
  `view_clients` int(11) NOT NULL DEFAULT '0',
  `delete_clients` int(11) NOT NULL DEFAULT '0',
  `add_news` int(11) NOT NULL DEFAULT '0',
  `delete_news` int(11) NOT NULL DEFAULT '0',
  `add_category` int(11) NOT NULL DEFAULT '0',
  `delete_category` int(11) NOT NULL DEFAULT '0',
  `view_admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reg_admin`
--

INSERT INTO `reg_admin` (`id`, `login`, `pass`, `fio`, `role`, `email`, `phone`, `view_orders`, `accept_orders`, `delete_orders`, `add_tovar`, `edit_tovar`, `delete_tovar`, `accept_reviews`, `delete_reviews`, `view_clients`, `delete_clients`, `add_news`, `delete_news`, `add_category`, `delete_category`, `view_admin`) VALUES
(2, 'admin', 'mb03foo5139a7d9937942b54253255931b44f7924qj2jjdp9', 'Жур Артур Владимирович', 'Администратор', 'test@mail.ru', '5678453', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 'administrator', 'administrator', '', 'Администратор', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `reg_user`
--

CREATE TABLE `reg_user` (
  `id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `patronymic` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `datetime` datetime NOT NULL,
  `ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reg_user`
--

INSERT INTO `reg_user` (`id`, `login`, `pass`, `surname`, `name`, `patronymic`, `email`, `phone`, `address`, `datetime`, `ip`) VALUES
(5, 'admin', '9nm2rv8qf698afc1edfeb72368a386705cffcf482yo6z', 'Жур', 'Артур', 'Владимирович', 'test@mail.ru', '1232', 'г Литва', '2013-09-10 08:52:08', '127.0.0.1'),
(6, 'fefew', '9nm2rv8q74dac0993f6b2b321fd77409fe90464b2yo6z', '3213', 'efwefewfwe', 'wefewfw', '01.0198@mail.ru', '+380976622921', 'dsfdsfdsfsd', '2017-05-09 17:44:49', '127.0.0.1'),
(7, 'kirill98402', '9nm2rv8q8ef620e8a5ca44d593c74319e04591f02yo6z', '', '', '', 'kirill98402@mail.ru', 'sdfdsf', 'sdfds', '2017-05-17 18:53:21', '127.0.0.1'),
(8, 'sadasd32432', '9nm2rv8q95c23861fc9edf561ea2bba07c0d58832yo6z', 'dfgdsgdsggd', 'sdfdsfsdf', 'safsafasf', 'kishka@mail.ru', 'sfsdfdsfsd', 'fcsdfsdfsd', '2017-05-17 19:36:05', '127.0.0.1'),
(9, 'kiril548', '9nm2rv8q40bd42b735cedc47feb5c306090ceed22yo6z', 'Kharkevich', 'Kiril', 'Andreyevich', 'kirill98402@gmail.com', '+380976622921', 'Berdichev', '2017-05-24 19:21:04', '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблицы `table_products`
--

CREATE TABLE `table_products` (
  `products_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `seo_words` text NOT NULL,
  `seo_description` text NOT NULL,
  `mini_description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `mini_features` text NOT NULL,
  `features` text NOT NULL,
  `datetime` datetime NOT NULL,
  `new` int(11) NOT NULL DEFAULT '0',
  `leader` int(11) NOT NULL DEFAULT '0',
  `sale` int(11) NOT NULL DEFAULT '0',
  `visible` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `type_tovara` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `yes_like` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `table_products`
--

INSERT INTO `table_products` (`products_id`, `title`, `price`, `brand`, `seo_words`, `seo_description`, `mini_description`, `image`, `description`, `mini_features`, `features`, `datetime`, `new`, `leader`, `sale`, `visible`, `count`, `type_tovara`, `brand_id`, `yes_like`) VALUES
(17, 'Электроды ЦЛ-11', 100, 'Для сварки нержавеющих сталей', 'Электроды ЦЛ-11', 'Электроды ЦЛ-11', '<p>Стильный смартфон Philips W732 порадует широкой функциональностью. Модель поддерживает работу двух сим-карт, благодаря чему появляется возможность отделить личные звонки от рабочих</p>\r\n', 'mobile-1732.jpg', '<p>Смартфон <strong>Samsung I9500 Galaxy S4 16Gb</strong> представляет собой современное устройство для связи и общения. Широкий набор дополнительных функций позволяет девайсу заменить пользователю сразу несколько устройств, обеспечив комфортную навигацию, веб-серфинг и качественную съемку.</p>\r\n\r\n<p>Широкий выбор средств связи. Для выхода в Интернет смартфон предлагает модуль Wi-Fi и 3G-модем. Способ подключения можно выбирать в соответствии с текущими условиями &ndash; это обеспечивает практически повсеместный доступ к связи и экономный расход денежных средств. Также доступно соединение посредством Bluetooth. Для работы с электронной почтой смартфон имеет собственное E-mail-приложение.</p>\r\n\r\n<p>Быстрая ориентация в незнакомом месте. Смартфон Samsung I9500 Galaxy S4 16Gb оснащен GPS-навигатором, который поможет выбрать оптимальный маршрут и быстро найти поблизости нужный объект.</p>\r\n\r\n<p>Мощная камера. Фотокамера с разрешением матрицы 13 Мп позволяет сделать снимки высокого качества. Также поддерживается режим видеосъемки в формате Full HD. Светодиодная вспышка и автофокус всегда позволят получить хороший результат.</p>\r\n', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип электрода</td>\r\n			<td>Металлический</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип покрытого электрода по применению</td>\r\n			<td>Для сварки высоколегированных сталей</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>Типичные механические свойства металла шва</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Временное сопротивление sв, МПа</td>\r\n			<td>\r\n			<p>Предел текучести sт, МПа</p>\r\n			</td>\r\n			<td>\r\n			<p>Относительное удлинение d5, %</p>\r\n			</td>\r\n			<td>\r\n			<p>Ударная вязкость aн, Дж/см2</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>660</p>\r\n			</td>\r\n			<td>\r\n			<p>420</p>\r\n			</td>\r\n			<td>\r\n			<p>34</p>\r\n			</td>\r\n			<td>\r\n			<p>120</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Типичный химический состав наплавленного металла, %</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>C</td>\r\n			<td>\r\n			<p>Mn</p>\r\n			</td>\r\n			<td>\r\n			<p>Si</p>\r\n			</td>\r\n			<td>\r\n			<p>Ni</p>\r\n			</td>\r\n			<td>\r\n			<p>Cr</p>\r\n			</td>\r\n			<td>\r\n			<p>Nb</p>\r\n			</td>\r\n			<td>\r\n			<p>S</p>\r\n			</td>\r\n			<td>\r\n			<p>P</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>0,10</p>\r\n			</td>\r\n			<td>\r\n			<p>1,8</p>\r\n			</td>\r\n			<td>\r\n			<p>0,53</p>\r\n			</td>\r\n			<td>\r\n			<p>9,8</p>\r\n			</td>\r\n			<td>\r\n			<p>20,8</p>\r\n			</td>\r\n			<td>\r\n			<p>0,99</p>\r\n			</td>\r\n			<td>\r\n			<p>0,011</p>\r\n			</td>\r\n			<td>\r\n			<p>0,020</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Сварка в различных положениях и сила тока, А</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Диаметр, мм</td>\r\n			<td>\r\n			<p>Нижнее</p>\r\n			</td>\r\n			<td>\r\n			<p>Вертикальное</p>\r\n			</td>\r\n			<td>\r\n			<p>Потолочное</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2,0</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;55</p>\r\n			</td>\r\n			<td>\r\n			<p>30&ndash;40</p>\r\n			</td>\r\n			<td>\r\n			<p>30&ndash;40</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2,5</p>\r\n			</td>\r\n			<td>\r\n			<p>55&ndash;65</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;50</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;50</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>3,0</p>\r\n			</td>\r\n			<td>\r\n			<p>70&ndash;90</p>\r\n			</td>\r\n			<td>\r\n			<p>50&ndash;80</p>\r\n			</td>\r\n			<td>\r\n			<p>50&ndash;80</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>4,0</p>\r\n			</td>\r\n			<td>\r\n			<p>130&ndash;155</p>\r\n			</td>\r\n			<td>\r\n			<p>110&ndash;130</p>\r\n			</td>\r\n			<td>\r\n			<p>110&ndash;130</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>5,0</p>\r\n			</td>\r\n			<td>\r\n			<p>150&ndash;180</p>\r\n			</td>\r\n			<td>\r\n			<p>120&ndash;160</p>\r\n			</td>\r\n			<td>\r\n			<p>&ndash;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Особые свойства</p>\r\n\r\n<p>Электроды ЦЛ-11&nbsp;обеспечивают получение металла шва, стойкого к межкристаллитной коррозии при испытаниях по методу АМУ ГОСТ 6032-89 без провоцирующего отпуска. Содержание ферритной фазы в наплавленном металле 2,5-10% (типичное 6,1%).</p>\r\n\r\n<p>Технологические особенности сварки<br />\r\nСварка после обязательной прокалки: 190-210&deg;С; 1 ч.</p>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 8, 'mobile', 1, 1),
(18, 'Электроды ОЗЛ-6', 115, 'Для сварки нержавеющих сталей', 'Электроды ОЗЛ-6', 'Электроды ОЗЛ-6', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип электрода</td>\r\n			<td>Металлический</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип покрытого электрода по применению</td>\r\n			<td>Для сварки высоколегированных сталей</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'mobile-1883.jpg', '<p><strong>Электроды ОЗЛ-6</strong>&nbsp;: сварка оборудования из литья и проката жаростойких сталей марок 20Х23Н13, 20Х23Н18 и им подобных, работающего в окислительных средах при температуре до 1000&deg;С. Возможна сварка&nbsp;<a href=\"https://prom.ua/redirect?url=http%3A%2F%2Fweldzone.info%2Ftechnology%2Fmaterials%2F49-carbonic%2F790-xromistye-stali\" target=\"_blank\">хромистых сталей</a>&nbsp;типа 15Х25Т и стали марки 25Х25Н20С2, а также сварка углеродистых и низколегированных сталей с высоколегированными аустенитными сталями.&nbsp;<br />\r\nСварка в нижнем, вертикальном и потолочном положениях шва постоянным током обратной полярности.</p>\r\n', '<p>Характеристика электродов<br />\r\nПокрытие - основное.<br />\r\nКоэффициент наплавки - 11,5 г/А&bull; ч.<br />\r\nПроизводительность наплавки (для диаметра 4,0 мм) - 1,5 кг/ч.<br />\r\nРасход электродов на 1 кг наплавленного металла - 1,6 кг.</p>\r\n', '<p>Типичные механические свойства металла шва</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Временное сопротивление sв, МПа</td>\r\n			<td>\r\n			<p>Предел текучести sт, МПа</p>\r\n			</td>\r\n			<td>\r\n			<p>Относительное удлинение d5, %</p>\r\n			</td>\r\n			<td>\r\n			<p>Ударная вязкость aн, Дж/см2</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>610</p>\r\n			</td>\r\n			<td>\r\n			<p>410</p>\r\n			</td>\r\n			<td>\r\n			<p>33</p>\r\n			</td>\r\n			<td>\r\n			<p>150</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Типичный химический состав наплавленного металла, %</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>C</td>\r\n			<td>\r\n			<p>Mn</p>\r\n			</td>\r\n			<td>\r\n			<p>Si</p>\r\n			</td>\r\n			<td>\r\n			<p>Ni</p>\r\n			</td>\r\n			<td>\r\n			<p>Cr</p>\r\n			</td>\r\n			<td>\r\n			<p>S</p>\r\n			</td>\r\n			<td>\r\n			<p>P</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>0,09</p>\r\n			</td>\r\n			<td>\r\n			<p>1,9</p>\r\n			</td>\r\n			<td>\r\n			<p>0,38</p>\r\n			</td>\r\n			<td>\r\n			<p>12,8</p>\r\n			</td>\r\n			<td>\r\n			<p>24,9</p>\r\n			</td>\r\n			<td>\r\n			<p>0,011</p>\r\n			</td>\r\n			<td>\r\n			<p>0,022</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Сварка в различных положениях и сила тока, А</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td rowspan=\"2\">Диаметр, мм</td>\r\n			<td rowspan=\"2\">\r\n			<p>Длина, мм</p>\r\n			</td>\r\n			<td colspan=\"3\">\r\n			<p>Рекомендуемое значение сварочного тока, А</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Нижнее</p>\r\n			</td>\r\n			<td>\r\n			<p>Вертикальное</p>\r\n			</td>\r\n			<td>\r\n			<p>потолочное</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>3,0</p>\r\n			</td>\r\n			<td>\r\n			<p>300</p>\r\n			</td>\r\n			<td>\r\n			<p>60-80</p>\r\n			</td>\r\n			<td>\r\n			<p>50-70</p>\r\n			</td>\r\n			<td>\r\n			<p>50-70</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>4,0</p>\r\n			</td>\r\n			<td>\r\n			<p>300</p>\r\n			</td>\r\n			<td>\r\n			<p>120-140</p>\r\n			</td>\r\n			<td>\r\n			<p>100-120</p>\r\n			</td>\r\n			<td>\r\n			<p>100-110</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>5,0</p>\r\n			</td>\r\n			<td>\r\n			<p>300</p>\r\n			</td>\r\n			<td>\r\n			<p>140-160</p>\r\n			</td>\r\n			<td>\r\n			<p>120-140</p>\r\n			</td>\r\n			<td>\r\n			<p>-</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Особые свойства<br />\r\nЭлектроды ОЗЛ-6&nbsp;обеспечивают получение металла шва с высокой жаростойкостью до температуры 1000&deg;С и стойкостью к межкристаллитной коррозии при испытаниях по методу АМУ ГОСТ 6032-89. В температурном интервале сигматизации металл шва может приобретать склонность к охрупчиванию. Содержание ферритной фазы в наплавленном металле 2,5-10% (типичное 5,3%).</p>\r\n\r\n<p>Технологические особенности сварки<br />\r\nСварка после обязательной прокалки: 190-210&deg;С; 1 ч.</p>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 0, 'mobile', 1, 1),
(19, 'Проволока СВ-08Г2С омедненная', 27, 'Для сварки углеродистых и низкоуглеродистых сплавов', 'Проволока СВ-08Г2С омедненная', 'Проволока СВ-08Г2С омедненная', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип сварочной проволоки</td>\r\n			<td>Сплошного сечения</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип сварочной проволоки по применению</td>\r\n			<td>Для механизированной сварки MIG/MAG</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип проволоки по содержанию углерода и легирующих элементов</td>\r\n			<td>Низкоуглеродистая</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Вид сварочной проволоки</td>\r\n			<td>Омедненная</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'notebook-1927.jpg', '<p><strong>Омедненная сварочная проволока СВ08Г2С</strong>&nbsp;(Св-08Г2С) разработана для сварки при силе тока до 600 А. Лучшим вариантом газовой смеси для нее считается смесь аргона и углекислого газа. Сварочная проволока для полуавтоматов Св-08Г2С применяеться во многих отраслях промышленности: в машиностроении, подъемно-транспортном машиностроении, судостроении и др.</p>\r\n', '<p>Проволока СВ08Г2С изготавливается в соответствии с требованиями ГОСТ 2246-70, что обеспечивает стабильные характеристики. Производство проволоки св08г2с происходит из высококачественных материалов с применением самых современных технологий, что позволяет подделживать отменное качество и постоянство состава.</p>\r\n', '<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Диаметр проволоки</td>\r\n			<td colspan=\"2\">0,8 мм</td>\r\n			<td colspan=\"2\">1,0 мм&nbsp;</td>\r\n			<td colspan=\"2\">1,2 мм</td>\r\n			<td>1,6 мм</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Вес кассеты</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>15 кг.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Диаметр кассеты, мм</td>\r\n			<td>200&nbsp;</td>\r\n			<td>300</td>\r\n			<td>200</td>\r\n			<td>300</td>\r\n			<td>200</td>\r\n			<td>300</td>\r\n			<td>300</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Проволока СВ08Г2С изготавливается в соответствии с требованиями ГОСТ 2246-70, что обеспечивает стабильные характеристики. Производство проволоки св08г2с происходит из высококачественных материалов с применением самых современных технологий, что позволяет подделживать отменное качество и постоянство состава.</p>\r\n\r\n<p>Стабильное горение дуги обеспечивается низким содержанием примесей, благодоря чему улучшаются характеристики шва и слабое разбрызгивание.&nbsp;<a href=\"https://prom.ua/redirect?url=http%3A%2F%2Fsaturn-sv.ru%2Fprovoloka-svarochnaya\" target=\"_blank\">Проволока сварочная</a>&nbsp;св08г2с поставляется в катушках с рядной намоткой. Рядная намотка благоприятно влияет на срок службы сварочных аппаратов.</p>\r\n\r\n<h2>Химический состав сварочной проволоки СВ-08Г2С</h2>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Марка</strong></td>\r\n			<td><strong>С<br />\r\n			углерод</strong></td>\r\n			<td><strong>Mn<br />\r\n			марганец</strong></td>\r\n			<td><strong>Si<br />\r\n			кремний</strong></td>\r\n			<td><strong>S<br />\r\n			Сера</strong></td>\r\n			<td><strong>P<br />\r\n			Фосфор</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Св-08Г2С</td>\r\n			<td>0.060<br />\r\n			&nbsp;</td>\r\n			<td>1.80</td>\r\n			<td>0.88</td>\r\n			<td>0.012</td>\r\n			<td>0.010</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Прочностные характеристики</h2>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Защитный газ</td>\r\n			<td>Предел прочности, МПа.</td>\r\n			<td>Предел&nbsp;<br />\r\n			текучести, МПа.</td>\r\n			<td>Относительное удлинение, %</td>\r\n			<td>Ударная вязкость, Дш/см2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>СО2</td>\r\n			<td>540</td>\r\n			<td>440</td>\r\n			<td>29</td>\r\n			<td>200</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Смесь 80% Ar + 20% CO2</td>\r\n			<td>550</td>\r\n			<td>422</td>\r\n			<td>29</td>\r\n			<td>200<br />\r\n			&nbsp;&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 1, 'notebook', 9, 1),
(20, 'Электроды ЦЛ-11', 100, 'Для сварки нержавеющих сталей', 'Электроды ЦЛ-11', 'Электроды ЦЛ-11', '<p>Стильный смартфон Philips W732 порадует широкой функциональностью. Модель поддерживает работу двух сим-карт, благодаря чему появляется возможность отделить личные звонки от рабочих</p>\r\n', 'mobile-1732.jpg', '<p>Смартфон <strong>Samsung I9500 Galaxy S4 16Gb</strong> представляет собой современное устройство для связи и общения. Широкий набор дополнительных функций позволяет девайсу заменить пользователю сразу несколько устройств, обеспечив комфортную навигацию, веб-серфинг и качественную съемку.</p>\r\n\r\n<p>Широкий выбор средств связи. Для выхода в Интернет смартфон предлагает модуль Wi-Fi и 3G-модем. Способ подключения можно выбирать в соответствии с текущими условиями &ndash; это обеспечивает практически повсеместный доступ к связи и экономный расход денежных средств. Также доступно соединение посредством Bluetooth. Для работы с электронной почтой смартфон имеет собственное E-mail-приложение.</p>\r\n\r\n<p>Быстрая ориентация в незнакомом месте. Смартфон Samsung I9500 Galaxy S4 16Gb оснащен GPS-навигатором, который поможет выбрать оптимальный маршрут и быстро найти поблизости нужный объект.</p>\r\n\r\n<p>Мощная камера. Фотокамера с разрешением матрицы 13 Мп позволяет сделать снимки высокого качества. Также поддерживается режим видеосъемки в формате Full HD. Светодиодная вспышка и автофокус всегда позволят получить хороший результат.</p>\r\n', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип электрода</td>\r\n			<td>Металлический</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип покрытого электрода по применению</td>\r\n			<td>Для сварки высоколегированных сталей</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>Типичные механические свойства металла шва</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Временное сопротивление sв, МПа</td>\r\n			<td>\r\n			<p>Предел текучести sт, МПа</p>\r\n			</td>\r\n			<td>\r\n			<p>Относительное удлинение d5, %</p>\r\n			</td>\r\n			<td>\r\n			<p>Ударная вязкость aн, Дж/см2</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>660</p>\r\n			</td>\r\n			<td>\r\n			<p>420</p>\r\n			</td>\r\n			<td>\r\n			<p>34</p>\r\n			</td>\r\n			<td>\r\n			<p>120</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Типичный химический состав наплавленного металла, %</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>C</td>\r\n			<td>\r\n			<p>Mn</p>\r\n			</td>\r\n			<td>\r\n			<p>Si</p>\r\n			</td>\r\n			<td>\r\n			<p>Ni</p>\r\n			</td>\r\n			<td>\r\n			<p>Cr</p>\r\n			</td>\r\n			<td>\r\n			<p>Nb</p>\r\n			</td>\r\n			<td>\r\n			<p>S</p>\r\n			</td>\r\n			<td>\r\n			<p>P</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>0,10</p>\r\n			</td>\r\n			<td>\r\n			<p>1,8</p>\r\n			</td>\r\n			<td>\r\n			<p>0,53</p>\r\n			</td>\r\n			<td>\r\n			<p>9,8</p>\r\n			</td>\r\n			<td>\r\n			<p>20,8</p>\r\n			</td>\r\n			<td>\r\n			<p>0,99</p>\r\n			</td>\r\n			<td>\r\n			<p>0,011</p>\r\n			</td>\r\n			<td>\r\n			<p>0,020</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Сварка в различных положениях и сила тока, А</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Диаметр, мм</td>\r\n			<td>\r\n			<p>Нижнее</p>\r\n			</td>\r\n			<td>\r\n			<p>Вертикальное</p>\r\n			</td>\r\n			<td>\r\n			<p>Потолочное</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2,0</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;55</p>\r\n			</td>\r\n			<td>\r\n			<p>30&ndash;40</p>\r\n			</td>\r\n			<td>\r\n			<p>30&ndash;40</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2,5</p>\r\n			</td>\r\n			<td>\r\n			<p>55&ndash;65</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;50</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;50</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>3,0</p>\r\n			</td>\r\n			<td>\r\n			<p>70&ndash;90</p>\r\n			</td>\r\n			<td>\r\n			<p>50&ndash;80</p>\r\n			</td>\r\n			<td>\r\n			<p>50&ndash;80</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>4,0</p>\r\n			</td>\r\n			<td>\r\n			<p>130&ndash;155</p>\r\n			</td>\r\n			<td>\r\n			<p>110&ndash;130</p>\r\n			</td>\r\n			<td>\r\n			<p>110&ndash;130</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>5,0</p>\r\n			</td>\r\n			<td>\r\n			<p>150&ndash;180</p>\r\n			</td>\r\n			<td>\r\n			<p>120&ndash;160</p>\r\n			</td>\r\n			<td>\r\n			<p>&ndash;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Особые свойства</p>\r\n\r\n<p>Электроды ЦЛ-11&nbsp;обеспечивают получение металла шва, стойкого к межкристаллитной коррозии при испытаниях по методу АМУ ГОСТ 6032-89 без провоцирующего отпуска. Содержание ферритной фазы в наплавленном металле 2,5-10% (типичное 6,1%).</p>\r\n\r\n<p>Технологические особенности сварки<br />\r\nСварка после обязательной прокалки: 190-210&deg;С; 1 ч.</p>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 9, 'mobile', 1, 1),
(21, 'Проволока СВ-08Г2С омедненная', 27, 'Для сварки углеродистых и низкоуглеродистых сплавов', 'Проволока СВ-08Г2С омедненная', 'Проволока СВ-08Г2С омедненная', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип сварочной проволоки</td>\r\n			<td>Сплошного сечения</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип сварочной проволоки по применению</td>\r\n			<td>Для механизированной сварки MIG/MAG</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип проволоки по содержанию углерода и легирующих элементов</td>\r\n			<td>Низкоуглеродистая</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Вид сварочной проволоки</td>\r\n			<td>Омедненная</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'notebook-1927.jpg', '<p><strong>Омедненная сварочная проволока СВ08Г2С</strong>&nbsp;(Св-08Г2С) разработана для сварки при силе тока до 600 А. Лучшим вариантом газовой смеси для нее считается смесь аргона и углекислого газа. Сварочная проволока для полуавтоматов Св-08Г2С применяеться во многих отраслях промышленности: в машиностроении, подъемно-транспортном машиностроении, судостроении и др.</p>\r\n', '<p>Проволока СВ08Г2С изготавливается в соответствии с требованиями ГОСТ 2246-70, что обеспечивает стабильные характеристики. Производство проволоки св08г2с происходит из высококачественных материалов с применением самых современных технологий, что позволяет подделживать отменное качество и постоянство состава.</p>\r\n', '<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Диаметр проволоки</td>\r\n			<td colspan=\"2\">0,8 мм</td>\r\n			<td colspan=\"2\">1,0 мм&nbsp;</td>\r\n			<td colspan=\"2\">1,2 мм</td>\r\n			<td>1,6 мм</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Вес кассеты</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>15 кг.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Диаметр кассеты, мм</td>\r\n			<td>200&nbsp;</td>\r\n			<td>300</td>\r\n			<td>200</td>\r\n			<td>300</td>\r\n			<td>200</td>\r\n			<td>300</td>\r\n			<td>300</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Проволока СВ08Г2С изготавливается в соответствии с требованиями ГОСТ 2246-70, что обеспечивает стабильные характеристики. Производство проволоки св08г2с происходит из высококачественных материалов с применением самых современных технологий, что позволяет подделживать отменное качество и постоянство состава.</p>\r\n\r\n<p>Стабильное горение дуги обеспечивается низким содержанием примесей, благодоря чему улучшаются характеристики шва и слабое разбрызгивание.&nbsp;<a href=\"https://prom.ua/redirect?url=http%3A%2F%2Fsaturn-sv.ru%2Fprovoloka-svarochnaya\" target=\"_blank\">Проволока сварочная</a>&nbsp;св08г2с поставляется в катушках с рядной намоткой. Рядная намотка благоприятно влияет на срок службы сварочных аппаратов.</p>\r\n\r\n<h2>Химический состав сварочной проволоки СВ-08Г2С</h2>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Марка</strong></td>\r\n			<td><strong>С<br />\r\n			углерод</strong></td>\r\n			<td><strong>Mn<br />\r\n			марганец</strong></td>\r\n			<td><strong>Si<br />\r\n			кремний</strong></td>\r\n			<td><strong>S<br />\r\n			Сера</strong></td>\r\n			<td><strong>P<br />\r\n			Фосфор</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Св-08Г2С</td>\r\n			<td>0.060<br />\r\n			&nbsp;</td>\r\n			<td>1.80</td>\r\n			<td>0.88</td>\r\n			<td>0.012</td>\r\n			<td>0.010</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Прочностные характеристики</h2>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Защитный газ</td>\r\n			<td>Предел прочности, МПа.</td>\r\n			<td>Предел&nbsp;<br />\r\n			текучести, МПа.</td>\r\n			<td>Относительное удлинение, %</td>\r\n			<td>Ударная вязкость, Дш/см2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>СО2</td>\r\n			<td>540</td>\r\n			<td>440</td>\r\n			<td>29</td>\r\n			<td>200</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Смесь 80% Ar + 20% CO2</td>\r\n			<td>550</td>\r\n			<td>422</td>\r\n			<td>29</td>\r\n			<td>200<br />\r\n			&nbsp;&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 1, 'notebook', 9, 1),
(22, 'Электроды ЦЛ-11', 100, 'Для сварки нержавеющих сталей', 'Электроды ЦЛ-11', 'Электроды ЦЛ-11', '<p>Стильный смартфон Philips W732 порадует широкой функциональностью. Модель поддерживает работу двух сим-карт, благодаря чему появляется возможность отделить личные звонки от рабочих</p>\r\n', 'mobile-1732.jpg', '<p>Смартфон <strong>Samsung I9500 Galaxy S4 16Gb</strong> представляет собой современное устройство для связи и общения. Широкий набор дополнительных функций позволяет девайсу заменить пользователю сразу несколько устройств, обеспечив комфортную навигацию, веб-серфинг и качественную съемку.</p>\r\n\r\n<p>Широкий выбор средств связи. Для выхода в Интернет смартфон предлагает модуль Wi-Fi и 3G-модем. Способ подключения можно выбирать в соответствии с текущими условиями &ndash; это обеспечивает практически повсеместный доступ к связи и экономный расход денежных средств. Также доступно соединение посредством Bluetooth. Для работы с электронной почтой смартфон имеет собственное E-mail-приложение.</p>\r\n\r\n<p>Быстрая ориентация в незнакомом месте. Смартфон Samsung I9500 Galaxy S4 16Gb оснащен GPS-навигатором, который поможет выбрать оптимальный маршрут и быстро найти поблизости нужный объект.</p>\r\n\r\n<p>Мощная камера. Фотокамера с разрешением матрицы 13 Мп позволяет сделать снимки высокого качества. Также поддерживается режим видеосъемки в формате Full HD. Светодиодная вспышка и автофокус всегда позволят получить хороший результат.</p>\r\n', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип электрода</td>\r\n			<td>Металлический</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип покрытого электрода по применению</td>\r\n			<td>Для сварки высоколегированных сталей</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>Типичные механические свойства металла шва</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Временное сопротивление sв, МПа</td>\r\n			<td>\r\n			<p>Предел текучести sт, МПа</p>\r\n			</td>\r\n			<td>\r\n			<p>Относительное удлинение d5, %</p>\r\n			</td>\r\n			<td>\r\n			<p>Ударная вязкость aн, Дж/см2</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>660</p>\r\n			</td>\r\n			<td>\r\n			<p>420</p>\r\n			</td>\r\n			<td>\r\n			<p>34</p>\r\n			</td>\r\n			<td>\r\n			<p>120</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Типичный химический состав наплавленного металла, %</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>C</td>\r\n			<td>\r\n			<p>Mn</p>\r\n			</td>\r\n			<td>\r\n			<p>Si</p>\r\n			</td>\r\n			<td>\r\n			<p>Ni</p>\r\n			</td>\r\n			<td>\r\n			<p>Cr</p>\r\n			</td>\r\n			<td>\r\n			<p>Nb</p>\r\n			</td>\r\n			<td>\r\n			<p>S</p>\r\n			</td>\r\n			<td>\r\n			<p>P</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>0,10</p>\r\n			</td>\r\n			<td>\r\n			<p>1,8</p>\r\n			</td>\r\n			<td>\r\n			<p>0,53</p>\r\n			</td>\r\n			<td>\r\n			<p>9,8</p>\r\n			</td>\r\n			<td>\r\n			<p>20,8</p>\r\n			</td>\r\n			<td>\r\n			<p>0,99</p>\r\n			</td>\r\n			<td>\r\n			<p>0,011</p>\r\n			</td>\r\n			<td>\r\n			<p>0,020</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Сварка в различных положениях и сила тока, А</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Диаметр, мм</td>\r\n			<td>\r\n			<p>Нижнее</p>\r\n			</td>\r\n			<td>\r\n			<p>Вертикальное</p>\r\n			</td>\r\n			<td>\r\n			<p>Потолочное</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2,0</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;55</p>\r\n			</td>\r\n			<td>\r\n			<p>30&ndash;40</p>\r\n			</td>\r\n			<td>\r\n			<p>30&ndash;40</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2,5</p>\r\n			</td>\r\n			<td>\r\n			<p>55&ndash;65</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;50</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;50</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>3,0</p>\r\n			</td>\r\n			<td>\r\n			<p>70&ndash;90</p>\r\n			</td>\r\n			<td>\r\n			<p>50&ndash;80</p>\r\n			</td>\r\n			<td>\r\n			<p>50&ndash;80</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>4,0</p>\r\n			</td>\r\n			<td>\r\n			<p>130&ndash;155</p>\r\n			</td>\r\n			<td>\r\n			<p>110&ndash;130</p>\r\n			</td>\r\n			<td>\r\n			<p>110&ndash;130</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>5,0</p>\r\n			</td>\r\n			<td>\r\n			<p>150&ndash;180</p>\r\n			</td>\r\n			<td>\r\n			<p>120&ndash;160</p>\r\n			</td>\r\n			<td>\r\n			<p>&ndash;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Особые свойства</p>\r\n\r\n<p>Электроды ЦЛ-11&nbsp;обеспечивают получение металла шва, стойкого к межкристаллитной коррозии при испытаниях по методу АМУ ГОСТ 6032-89 без провоцирующего отпуска. Содержание ферритной фазы в наплавленном металле 2,5-10% (типичное 6,1%).</p>\r\n\r\n<p>Технологические особенности сварки<br />\r\nСварка после обязательной прокалки: 190-210&deg;С; 1 ч.</p>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 8, 'mobile', 1, 1),
(23, 'Электроды ОЗЛ-6', 115, 'Для сварки нержавеющих сталей', 'Электроды ОЗЛ-6', 'Электроды ОЗЛ-6', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип электрода</td>\r\n			<td>Металлический</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип покрытого электрода по применению</td>\r\n			<td>Для сварки высоколегированных сталей</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'mobile-1883.jpg', '<p><strong>Электроды ОЗЛ-6</strong>&nbsp;: сварка оборудования из литья и проката жаростойких сталей марок 20Х23Н13, 20Х23Н18 и им подобных, работающего в окислительных средах при температуре до 1000&deg;С. Возможна сварка&nbsp;<a href=\"https://prom.ua/redirect?url=http%3A%2F%2Fweldzone.info%2Ftechnology%2Fmaterials%2F49-carbonic%2F790-xromistye-stali\" target=\"_blank\">хромистых сталей</a>&nbsp;типа 15Х25Т и стали марки 25Х25Н20С2, а также сварка углеродистых и низколегированных сталей с высоколегированными аустенитными сталями.&nbsp;<br />\r\nСварка в нижнем, вертикальном и потолочном положениях шва постоянным током обратной полярности.</p>\r\n', '<p>Характеристика электродов<br />\r\nПокрытие - основное.<br />\r\nКоэффициент наплавки - 11,5 г/А&bull; ч.<br />\r\nПроизводительность наплавки (для диаметра 4,0 мм) - 1,5 кг/ч.<br />\r\nРасход электродов на 1 кг наплавленного металла - 1,6 кг.</p>\r\n', '<p>Типичные механические свойства металла шва</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Временное сопротивление sв, МПа</td>\r\n			<td>\r\n			<p>Предел текучести sт, МПа</p>\r\n			</td>\r\n			<td>\r\n			<p>Относительное удлинение d5, %</p>\r\n			</td>\r\n			<td>\r\n			<p>Ударная вязкость aн, Дж/см2</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>610</p>\r\n			</td>\r\n			<td>\r\n			<p>410</p>\r\n			</td>\r\n			<td>\r\n			<p>33</p>\r\n			</td>\r\n			<td>\r\n			<p>150</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Типичный химический состав наплавленного металла, %</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>C</td>\r\n			<td>\r\n			<p>Mn</p>\r\n			</td>\r\n			<td>\r\n			<p>Si</p>\r\n			</td>\r\n			<td>\r\n			<p>Ni</p>\r\n			</td>\r\n			<td>\r\n			<p>Cr</p>\r\n			</td>\r\n			<td>\r\n			<p>S</p>\r\n			</td>\r\n			<td>\r\n			<p>P</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>0,09</p>\r\n			</td>\r\n			<td>\r\n			<p>1,9</p>\r\n			</td>\r\n			<td>\r\n			<p>0,38</p>\r\n			</td>\r\n			<td>\r\n			<p>12,8</p>\r\n			</td>\r\n			<td>\r\n			<p>24,9</p>\r\n			</td>\r\n			<td>\r\n			<p>0,011</p>\r\n			</td>\r\n			<td>\r\n			<p>0,022</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Сварка в различных положениях и сила тока, А</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td rowspan=\"2\">Диаметр, мм</td>\r\n			<td rowspan=\"2\">\r\n			<p>Длина, мм</p>\r\n			</td>\r\n			<td colspan=\"3\">\r\n			<p>Рекомендуемое значение сварочного тока, А</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Нижнее</p>\r\n			</td>\r\n			<td>\r\n			<p>Вертикальное</p>\r\n			</td>\r\n			<td>\r\n			<p>потолочное</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>3,0</p>\r\n			</td>\r\n			<td>\r\n			<p>300</p>\r\n			</td>\r\n			<td>\r\n			<p>60-80</p>\r\n			</td>\r\n			<td>\r\n			<p>50-70</p>\r\n			</td>\r\n			<td>\r\n			<p>50-70</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>4,0</p>\r\n			</td>\r\n			<td>\r\n			<p>300</p>\r\n			</td>\r\n			<td>\r\n			<p>120-140</p>\r\n			</td>\r\n			<td>\r\n			<p>100-120</p>\r\n			</td>\r\n			<td>\r\n			<p>100-110</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>5,0</p>\r\n			</td>\r\n			<td>\r\n			<p>300</p>\r\n			</td>\r\n			<td>\r\n			<p>140-160</p>\r\n			</td>\r\n			<td>\r\n			<p>120-140</p>\r\n			</td>\r\n			<td>\r\n			<p>-</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Особые свойства<br />\r\nЭлектроды ОЗЛ-6&nbsp;обеспечивают получение металла шва с высокой жаростойкостью до температуры 1000&deg;С и стойкостью к межкристаллитной коррозии при испытаниях по методу АМУ ГОСТ 6032-89. В температурном интервале сигматизации металл шва может приобретать склонность к охрупчиванию. Содержание ферритной фазы в наплавленном металле 2,5-10% (типичное 5,3%).</p>\r\n\r\n<p>Технологические особенности сварки<br />\r\nСварка после обязательной прокалки: 190-210&deg;С; 1 ч.</p>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 2, 'mobile', 1, 1),
(24, 'Проволока СВ-08Г2С омедненная', 27, 'Для сварки углеродистых и низкоуглеродистых сплавов', 'Проволока СВ-08Г2С омедненная', 'Проволока СВ-08Г2С омедненная', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип сварочной проволоки</td>\r\n			<td>Сплошного сечения</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип сварочной проволоки по применению</td>\r\n			<td>Для механизированной сварки MIG/MAG</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип проволоки по содержанию углерода и легирующих элементов</td>\r\n			<td>Низкоуглеродистая</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Вид сварочной проволоки</td>\r\n			<td>Омедненная</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'notebook-1927.jpg', '<p><strong>Омедненная сварочная проволока СВ08Г2С</strong>&nbsp;(Св-08Г2С) разработана для сварки при силе тока до 600 А. Лучшим вариантом газовой смеси для нее считается смесь аргона и углекислого газа. Сварочная проволока для полуавтоматов Св-08Г2С применяеться во многих отраслях промышленности: в машиностроении, подъемно-транспортном машиностроении, судостроении и др.</p>\r\n', '<p>Проволока СВ08Г2С изготавливается в соответствии с требованиями ГОСТ 2246-70, что обеспечивает стабильные характеристики. Производство проволоки св08г2с происходит из высококачественных материалов с применением самых современных технологий, что позволяет подделживать отменное качество и постоянство состава.</p>\r\n', '<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Диаметр проволоки</td>\r\n			<td colspan=\"2\">0,8 мм</td>\r\n			<td colspan=\"2\">1,0 мм&nbsp;</td>\r\n			<td colspan=\"2\">1,2 мм</td>\r\n			<td>1,6 мм</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Вес кассеты</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>15 кг.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Диаметр кассеты, мм</td>\r\n			<td>200&nbsp;</td>\r\n			<td>300</td>\r\n			<td>200</td>\r\n			<td>300</td>\r\n			<td>200</td>\r\n			<td>300</td>\r\n			<td>300</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Проволока СВ08Г2С изготавливается в соответствии с требованиями ГОСТ 2246-70, что обеспечивает стабильные характеристики. Производство проволоки св08г2с происходит из высококачественных материалов с применением самых современных технологий, что позволяет подделживать отменное качество и постоянство состава.</p>\r\n\r\n<p>Стабильное горение дуги обеспечивается низким содержанием примесей, благодоря чему улучшаются характеристики шва и слабое разбрызгивание.&nbsp;<a href=\"https://prom.ua/redirect?url=http%3A%2F%2Fsaturn-sv.ru%2Fprovoloka-svarochnaya\" target=\"_blank\">Проволока сварочная</a>&nbsp;св08г2с поставляется в катушках с рядной намоткой. Рядная намотка благоприятно влияет на срок службы сварочных аппаратов.</p>\r\n\r\n<h2>Химический состав сварочной проволоки СВ-08Г2С</h2>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Марка</strong></td>\r\n			<td><strong>С<br />\r\n			углерод</strong></td>\r\n			<td><strong>Mn<br />\r\n			марганец</strong></td>\r\n			<td><strong>Si<br />\r\n			кремний</strong></td>\r\n			<td><strong>S<br />\r\n			Сера</strong></td>\r\n			<td><strong>P<br />\r\n			Фосфор</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Св-08Г2С</td>\r\n			<td>0.060<br />\r\n			&nbsp;</td>\r\n			<td>1.80</td>\r\n			<td>0.88</td>\r\n			<td>0.012</td>\r\n			<td>0.010</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Прочностные характеристики</h2>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Защитный газ</td>\r\n			<td>Предел прочности, МПа.</td>\r\n			<td>Предел&nbsp;<br />\r\n			текучести, МПа.</td>\r\n			<td>Относительное удлинение, %</td>\r\n			<td>Ударная вязкость, Дш/см2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>СО2</td>\r\n			<td>540</td>\r\n			<td>440</td>\r\n			<td>29</td>\r\n			<td>200</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Смесь 80% Ar + 20% CO2</td>\r\n			<td>550</td>\r\n			<td>422</td>\r\n			<td>29</td>\r\n			<td>200<br />\r\n			&nbsp;&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 2, 'notebook', 9, 1),
(25, 'Электроды ЦЛ-11', 100, 'Для сварки нержавеющих сталей', 'Электроды ЦЛ-11', 'Электроды ЦЛ-11', '<p>Стильный смартфон Philips W732 порадует широкой функциональностью. Модель поддерживает работу двух сим-карт, благодаря чему появляется возможность отделить личные звонки от рабочих</p>\r\n', 'mobile-1732.jpg', '<p>Смартфон <strong>Samsung I9500 Galaxy S4 16Gb</strong> представляет собой современное устройство для связи и общения. Широкий набор дополнительных функций позволяет девайсу заменить пользователю сразу несколько устройств, обеспечив комфортную навигацию, веб-серфинг и качественную съемку.</p>\r\n\r\n<p>Широкий выбор средств связи. Для выхода в Интернет смартфон предлагает модуль Wi-Fi и 3G-модем. Способ подключения можно выбирать в соответствии с текущими условиями &ndash; это обеспечивает практически повсеместный доступ к связи и экономный расход денежных средств. Также доступно соединение посредством Bluetooth. Для работы с электронной почтой смартфон имеет собственное E-mail-приложение.</p>\r\n\r\n<p>Быстрая ориентация в незнакомом месте. Смартфон Samsung I9500 Galaxy S4 16Gb оснащен GPS-навигатором, который поможет выбрать оптимальный маршрут и быстро найти поблизости нужный объект.</p>\r\n\r\n<p>Мощная камера. Фотокамера с разрешением матрицы 13 Мп позволяет сделать снимки высокого качества. Также поддерживается режим видеосъемки в формате Full HD. Светодиодная вспышка и автофокус всегда позволят получить хороший результат.</p>\r\n', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип электрода</td>\r\n			<td>Металлический</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип покрытого электрода по применению</td>\r\n			<td>Для сварки высоколегированных сталей</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>Типичные механические свойства металла шва</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Временное сопротивление sв, МПа</td>\r\n			<td>\r\n			<p>Предел текучести sт, МПа</p>\r\n			</td>\r\n			<td>\r\n			<p>Относительное удлинение d5, %</p>\r\n			</td>\r\n			<td>\r\n			<p>Ударная вязкость aн, Дж/см2</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>660</p>\r\n			</td>\r\n			<td>\r\n			<p>420</p>\r\n			</td>\r\n			<td>\r\n			<p>34</p>\r\n			</td>\r\n			<td>\r\n			<p>120</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Типичный химический состав наплавленного металла, %</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>C</td>\r\n			<td>\r\n			<p>Mn</p>\r\n			</td>\r\n			<td>\r\n			<p>Si</p>\r\n			</td>\r\n			<td>\r\n			<p>Ni</p>\r\n			</td>\r\n			<td>\r\n			<p>Cr</p>\r\n			</td>\r\n			<td>\r\n			<p>Nb</p>\r\n			</td>\r\n			<td>\r\n			<p>S</p>\r\n			</td>\r\n			<td>\r\n			<p>P</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>0,10</p>\r\n			</td>\r\n			<td>\r\n			<p>1,8</p>\r\n			</td>\r\n			<td>\r\n			<p>0,53</p>\r\n			</td>\r\n			<td>\r\n			<p>9,8</p>\r\n			</td>\r\n			<td>\r\n			<p>20,8</p>\r\n			</td>\r\n			<td>\r\n			<p>0,99</p>\r\n			</td>\r\n			<td>\r\n			<p>0,011</p>\r\n			</td>\r\n			<td>\r\n			<p>0,020</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Сварка в различных положениях и сила тока, А</p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Диаметр, мм</td>\r\n			<td>\r\n			<p>Нижнее</p>\r\n			</td>\r\n			<td>\r\n			<p>Вертикальное</p>\r\n			</td>\r\n			<td>\r\n			<p>Потолочное</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2,0</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;55</p>\r\n			</td>\r\n			<td>\r\n			<p>30&ndash;40</p>\r\n			</td>\r\n			<td>\r\n			<p>30&ndash;40</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2,5</p>\r\n			</td>\r\n			<td>\r\n			<p>55&ndash;65</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;50</p>\r\n			</td>\r\n			<td>\r\n			<p>40&ndash;50</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>3,0</p>\r\n			</td>\r\n			<td>\r\n			<p>70&ndash;90</p>\r\n			</td>\r\n			<td>\r\n			<p>50&ndash;80</p>\r\n			</td>\r\n			<td>\r\n			<p>50&ndash;80</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>4,0</p>\r\n			</td>\r\n			<td>\r\n			<p>130&ndash;155</p>\r\n			</td>\r\n			<td>\r\n			<p>110&ndash;130</p>\r\n			</td>\r\n			<td>\r\n			<p>110&ndash;130</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>5,0</p>\r\n			</td>\r\n			<td>\r\n			<p>150&ndash;180</p>\r\n			</td>\r\n			<td>\r\n			<p>120&ndash;160</p>\r\n			</td>\r\n			<td>\r\n			<p>&ndash;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Особые свойства</p>\r\n\r\n<p>Электроды ЦЛ-11&nbsp;обеспечивают получение металла шва, стойкого к межкристаллитной коррозии при испытаниях по методу АМУ ГОСТ 6032-89 без провоцирующего отпуска. Содержание ферритной фазы в наплавленном металле 2,5-10% (типичное 6,1%).</p>\r\n\r\n<p>Технологические особенности сварки<br />\r\nСварка после обязательной прокалки: 190-210&deg;С; 1 ч.</p>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 9, 'mobile', 1, 1),
(26, 'Проволока СВ-08Г2С омедненная', 27, 'Для сварки углеродистых и низкоуглеродистых сплавов', 'Проволока СВ-08Г2С омедненная', 'Проволока СВ-08Г2С омедненная', '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Страна производитель</td>\r\n			<td>Украина</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип сварочной проволоки</td>\r\n			<td>Сплошного сечения</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип сварочной проволоки по применению</td>\r\n			<td>Для механизированной сварки MIG/MAG</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Тип проволоки по содержанию углерода и легирующих элементов</td>\r\n			<td>Низкоуглеродистая</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Вид сварочной проволоки</td>\r\n			<td>Омедненная</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'notebook-1927.jpg', '<p><strong>Омедненная сварочная проволока СВ08Г2С</strong>&nbsp;(Св-08Г2С) разработана для сварки при силе тока до 600 А. Лучшим вариантом газовой смеси для нее считается смесь аргона и углекислого газа. Сварочная проволока для полуавтоматов Св-08Г2С применяеться во многих отраслях промышленности: в машиностроении, подъемно-транспортном машиностроении, судостроении и др.</p>\r\n', '<p>Проволока СВ08Г2С изготавливается в соответствии с требованиями ГОСТ 2246-70, что обеспечивает стабильные характеристики. Производство проволоки св08г2с происходит из высококачественных материалов с применением самых современных технологий, что позволяет подделживать отменное качество и постоянство состава.</p>\r\n', '<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Диаметр проволоки</td>\r\n			<td colspan=\"2\">0,8 мм</td>\r\n			<td colspan=\"2\">1,0 мм&nbsp;</td>\r\n			<td colspan=\"2\">1,2 мм</td>\r\n			<td>1,6 мм</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Вес кассеты</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>5 кг.</td>\r\n			<td>15 кг.</td>\r\n			<td>15 кг.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Диаметр кассеты, мм</td>\r\n			<td>200&nbsp;</td>\r\n			<td>300</td>\r\n			<td>200</td>\r\n			<td>300</td>\r\n			<td>200</td>\r\n			<td>300</td>\r\n			<td>300</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Проволока СВ08Г2С изготавливается в соответствии с требованиями ГОСТ 2246-70, что обеспечивает стабильные характеристики. Производство проволоки св08г2с происходит из высококачественных материалов с применением самых современных технологий, что позволяет подделживать отменное качество и постоянство состава.</p>\r\n\r\n<p>Стабильное горение дуги обеспечивается низким содержанием примесей, благодоря чему улучшаются характеристики шва и слабое разбрызгивание.&nbsp;<a href=\"https://prom.ua/redirect?url=http%3A%2F%2Fsaturn-sv.ru%2Fprovoloka-svarochnaya\" target=\"_blank\">Проволока сварочная</a>&nbsp;св08г2с поставляется в катушках с рядной намоткой. Рядная намотка благоприятно влияет на срок службы сварочных аппаратов.</p>\r\n\r\n<h2>Химический состав сварочной проволоки СВ-08Г2С</h2>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Марка</strong></td>\r\n			<td><strong>С<br />\r\n			углерод</strong></td>\r\n			<td><strong>Mn<br />\r\n			марганец</strong></td>\r\n			<td><strong>Si<br />\r\n			кремний</strong></td>\r\n			<td><strong>S<br />\r\n			Сера</strong></td>\r\n			<td><strong>P<br />\r\n			Фосфор</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Св-08Г2С</td>\r\n			<td>0.060<br />\r\n			&nbsp;</td>\r\n			<td>1.80</td>\r\n			<td>0.88</td>\r\n			<td>0.012</td>\r\n			<td>0.010</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Прочностные характеристики</h2>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Защитный газ</td>\r\n			<td>Предел прочности, МПа.</td>\r\n			<td>Предел&nbsp;<br />\r\n			текучести, МПа.</td>\r\n			<td>Относительное удлинение, %</td>\r\n			<td>Ударная вязкость, Дш/см2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>СО2</td>\r\n			<td>540</td>\r\n			<td>440</td>\r\n			<td>29</td>\r\n			<td>200</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Смесь 80% Ar + 20% CO2</td>\r\n			<td>550</td>\r\n			<td>422</td>\r\n			<td>29</td>\r\n			<td>200<br />\r\n			&nbsp;&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '0000-00-00 00:00:00', 0, 0, 0, 1, 3, 'notebook', 9, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `table_reviews`
--

CREATE TABLE `table_reviews` (
  `reviews_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `good_reviews` text NOT NULL,
  `bad_reviews` text NOT NULL,
  `comment` text NOT NULL,
  `date` date NOT NULL,
  `moderat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `table_reviews`
--

INSERT INTO `table_reviews` (`reviews_id`, `products_id`, `name`, `good_reviews`, `bad_reviews`, `comment`, `date`, `moderat`) VALUES
(2, 17, 'Артур', 'Хороший телефон.Камера хорошая.', 'Жду когда появится все цвета телефонов.Скользкий иногда глючит.', 'Телефон использую 2-ой день,больше недостатков не нашел... Все самое интересное впереди :D', '2013-11-07', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `uploads_images`
--

CREATE TABLE `uploads_images` (
  `id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uploads_images`
--

INSERT INTO `uploads_images` (`id`, `products_id`, `image`) VALUES
(1, 17, 'mobile-227.jpg'),
(2, 17, 'mobile-170.jpg'),
(3, 19, 'notebook-346.jpg'),
(4, 19, 'notebook-444.png');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `buy_products`
--
ALTER TABLE `buy_products`
  ADD PRIMARY KEY (`buy_id`);

--
-- Индексы таблицы `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `reg_admin`
--
ALTER TABLE `reg_admin`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reg_user`
--
ALTER TABLE `reg_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `table_products`
--
ALTER TABLE `table_products`
  ADD PRIMARY KEY (`products_id`);

--
-- Индексы таблицы `table_reviews`
--
ALTER TABLE `table_reviews`
  ADD PRIMARY KEY (`reviews_id`);

--
-- Индексы таблицы `uploads_images`
--
ALTER TABLE `uploads_images`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `buy_products`
--
ALTER TABLE `buy_products`
  MODIFY `buy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `reg_admin`
--
ALTER TABLE `reg_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `reg_user`
--
ALTER TABLE `reg_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `table_products`
--
ALTER TABLE `table_products`
  MODIFY `products_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `table_reviews`
--
ALTER TABLE `table_reviews`
  MODIFY `reviews_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `uploads_images`
--
ALTER TABLE `uploads_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
